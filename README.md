
Para executar o projeto, será necessário instalar os seguintes programas:
JDK 14 ou acima: Necessário para executar o projeto Java
Maven 3.5.3: Necessário para realizar o build do projeto Java
IntelliJ: Para desenvolvimento do projeto
Navegador: Google Chrome
Para desenvolvimento ou execução dos testes, é necessário clonar o projeto do GitLab em um diretório de sua preferência
git clone git@gitlab.com:www.lucasrezendesilveira/prova_sicredi.git

